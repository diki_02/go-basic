package main

import (
	"fmt"
	"go_basic/gonew"
	"strings"
)

func main() {
	Angka := 2
	var kucing = [...]string{"kucing", "kucing hitam"}
	c := 0
	for i := 0; i < Angka; i++ {
		fmt.Println(i)
	}

	var jumlahKucing = len(kucing)
	for c < jumlahKucing {
		fmt.Println(kucing[c])
		c++
	}

	fmt.Println(gonew.StringOutput())

	slices := []string{"a", "b", "c"}
	fmt.Println(slices[2:3])

	var maptest = map[string]int{}

	maptest["kolom1"] = 1
	maptest["kolom2"] = 2

	var getHit1, getHit2 = multireturnFunc(20)

	var dataVariadic = variadicFunc(2, 4, 6, 2, 7, 8, 9, 7, 5, 4)

	var variadicWithSlice = []int{2, 4, 4, 6, 7, 8, 7, 6, 5}

	var hasilSlice = variadicFunc(variadicWithSlice...)

	var closureFunc = func(n []int) (int, int) {
		var a, b int
		for i, e := range n {
			switch {
			case i == 0:
				a, b = e, e
			case e > a:
				a = e
			case e < b:
				b = e
			}
		}
		return a, b
	}

	var listNumber = []int{1, 2, 3, 4, 5, 6, 7, 8}
	var min, max = closureFunc(listNumber)
	var closureIffe = func(min int) []int {
		var r []int
		for _, e := range listNumber {
			if e < min {
				continue
			}
			r = append(r, e)
		}

		return r
	}(3) // 3 yang nantinya jadi parameter min pada func di atas

	fmt.Println(min, max)

	fmt.Println(maptest)

	fmt.Println(slices[2:3:3])

	fmt.Println(getHit1, getHit2)

	fmt.Println(dataVariadic)
	fmt.Println(hasilSlice)
	fmt.Println("sebelum di filter: %v\n sesudah di filter: %v\n", listNumber, closureIffe)

	// fmt.Println(go_map.maptest)

}

func multireturnFunc(d int) (int, int) {
	var hitung = 10 * d
	var hitung2 = 20 * d

	return hitung, hitung2
}

func variadicFunc(numbers ...int) float64 {
	var totalValue = 0

	for _, number := range numbers {
		totalValue += number
	}

	var averageAngka = float64(totalValue) / float64(len(numbers))

	return averageAngka
}

func variadicFuncNew(nama string, data ...string) {
	var dataJoin = strings.Join(data, ",")

	fmt.Printf(dataJoin)
}
